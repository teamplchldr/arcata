﻿Shader "Custom/Line2Pass"
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,0.5)
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
		_Cutoff("Alpha cutoff", Range(0,0.9)) = 0.5
	}

		SubShader
		{
			Tags {"Queue" = "Transparent"}

			Pass
			{
				Tags { "LightMode" = "Vertex" }
				Alphatest Greater[_Cutoff]
				AlphaToMask True
				ColorMask RGB
				Blend SrcAlpha OneMinusSrcAlpha

				Material
				 {
					Diffuse[_Color]
					Ambient[_Color]
				}
				Lighting On
				SetTexture[_MainTex]
				{
					combine texture * primary double, texture * primary
				}
			}

			Pass
			{
				ZWrite Off
				ZTest Less
				Blend SrcAlpha OneMinusSrcAlpha

				Lighting Off
				SetTexture[_MainTex]
				{
					combine texture
				}
			}
		}

			Fallback "Transparent/Cutout/Soft Edge Unlit"
}