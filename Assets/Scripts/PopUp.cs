﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PopUp : MonoBehaviour
{

    public static bool GameIsPaused = false;
    public GameObject MenuPanel;
    public GameObject SettingMenuPanel;
    public static GameObject island;
    //public Slider Height;
    //public Slider Scale;
    //public Slider Rotate;


    public void PauseGame()
    {
        if(GameIsPaused)
        {
            Resume();
        }
        else {

            Pause();
        }
    }

    void Resume()
    {
        MenuPanel.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
    void Pause()
    {
        MenuPanel.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void Leave()
    {
        SceneManager.LoadScene("Lobby");
    }

    void Update()
    {
        Debug.Log("test"); 
    }

    public void Settings()
    {
        SettingMenuPanel.SetActive(true);
    }

    public void DiscardSettings()
    {

        SettingMenuPanel.SetActive(false);
    }

    public void ApplySettings()
    {
        SettingMenuPanel.SetActive(false);

    }
}
