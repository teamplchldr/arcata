﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Catapult Setup")]
    public float MovementSpeed = 5.0f;
    private float MaxWheelRotation = 360;
    private float RotateModifier = 20f;
    
    [Header("Front Wheels")]
    public Transform Catapult_Wheel_TopRight;
    public Transform Catapult_Wheel_TopLeft;

    [Header("Back Wheels")]
    public bool rotateBackWheels = false;
    public Transform Catapult_Wheel_BottomRight;
    public Transform Catapult_Wheel_BottomLeft;

    public void MovePlayer(Vector2 direction)
    {
        //direction = new Vector2(
        //    Mathf.Abs(direction.x) <= AbsDeadZones.x ? 0 : direction.x,
        //    Mathf.Abs(direction.y) <= AbsDeadZones.y ? 0 : direction.y);

        var translate = direction * MovementSpeed * Time.deltaTime;

        //if (translate.y > 0) translate.x = -translate.x;


        RotateWheels(translate.y > 0 ? translate.x : -translate.x);
        transform.Rotate(0, translate.x * RotateModifier, 0);
        transform.Translate(0, 0, translate.y);
    }

    public void RotateWheels(float horizontal, bool reset = false)
    {
        horizontal *= MaxWheelRotation;

        var newRotation = Quaternion.Euler(reset ? new Vector3(0, -90, 90) : new Vector3(0, -90 + horizontal, 90));

        Catapult_Wheel_TopRight.localRotation = newRotation;
        Catapult_Wheel_TopLeft.localRotation = newRotation;

        if (rotateBackWheels)
        {
            var newRotation2 = Quaternion.Euler(reset ? new Vector3(0, -90, 90) : new Vector3(0, -90 - horizontal, 90));

            Catapult_Wheel_BottomRight.localRotation = newRotation2;
            Catapult_Wheel_BottomLeft.localRotation = newRotation2;
        }
    }
}
