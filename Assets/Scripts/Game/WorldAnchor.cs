﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldAnchor : MonoBehaviour
{
    public float TargetScale { get; set; } = 1f;
    public float TargetHeight { get; set; } = 1f;
    public float TargetRotation { get; set; } = 0f;

    public Transform Island1Transform = null;
    public Transform Island2Transform = null;


    public void UpdateWorldAnchor()
    {
        //Scale
        if (Math.Abs(transform.localScale.x - TargetScale) > 0.01f)
        {
            transform.localScale = Vector3.Lerp(transform.localScale,
                new Vector3(TargetScale, TargetScale, TargetScale), Time.deltaTime);
        }

        //Height
        if (Math.Abs(transform.localPosition.y - TargetHeight) > 0.01f)
        {
            transform.localPosition =
                Vector3.Lerp(
                    transform.localPosition,
                    new Vector3(transform.localPosition.x, TargetHeight,
                        transform.localPosition.z),
                    Time.deltaTime);
        }

        //Rotation
        if (Math.Abs(transform.localEulerAngles.y - TargetRotation) > 0.01f)
        {
            transform.localEulerAngles =
                Vector3.Lerp(
                    transform.localEulerAngles,
                    new Vector3(transform.localEulerAngles.x, TargetRotation,
                        transform.localEulerAngles.z),
                    Time.deltaTime);
        }
    }
}
