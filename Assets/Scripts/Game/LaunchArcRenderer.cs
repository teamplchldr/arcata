﻿using UnityEngine;
using Assets.Scripts.Game.UI;
namespace Assets.Scripts {
    [RequireComponent(typeof(LineRenderer))]
    public class LaunchArcRenderer : MonoBehaviour
    {
        LineRenderer lr;
        public float velocity;
        public float angle;
        public Vector3 position;
        [SerializeField]
        public bool tempReverseLine = false;
        [SerializeField]
        public bool tempRenderLine = false; // Change to if photonView.IsMine

        public float debugVelocity = 0;
        public float debugAngle = 0;
        public float direction;
        public int resolution=40;
        public bool activated = false;
        private float maxDistance;

        private PlayerController playerController;
        public GameObject spoon;
        public ForcePuller forcePuller;
        private GameObject cam;
        private Vector3 rotation;
        private readonly float g = -Physics.gravity.y;
        private float radianAngle;
        

        void Awake()
        {
            
            lr = GetComponent<LineRenderer>();
            if (cam == null)
                cam = Camera.main.gameObject;
        
        }

        public void Ready()
        {
            
            spoon.GetComponent<SpoonCharge>().OnCharging += UpdateArc;
            forcePuller.OnReleaseCharge += DisableArc;

            rotation = Vector3.zero;
            activated = false;
        }

        private void OnDisable()
        {
            spoon.GetComponent<SpoonCharge>().OnCharging -= UpdateArc;
            if(forcePuller != null)
                forcePuller.OnReleaseCharge -= DisableArc;
        }

        private void DisableArc(float force)
        {
            activated = false;
            lr.enabled = false;
        }

        private void UpdateArc(float projectileForce, Vector3 up)
        {
            lr.startWidth = 0.2f + Vector3.Distance(transform.position, cam.transform.position) / 60;
            lr.endWidth = 0.2f + Vector3.Distance(transform.position, cam.transform.position) / 90;
            velocity = projectileForce;
        
            var dir = transform.TransformDirection(transform.forward).normalized * (tempReverseLine ? -1 : 1);
            var dirLocal = dir + up;
            rotation = dir;

            // Gets the angle between the vector dot on the xz plane, to y.
            var newDist = new Vector2(dirLocal.x, dirLocal.z).magnitude;
            angle = Mathf.Atan(dirLocal.y / newDist) * Mathf.Rad2Deg;

            Debug.Log("ProjF: " + velocity);
            Debug.Log("Start up: " + dirLocal);

            if (!tempRenderLine) return;
            activated = true;
            lr.enabled = true;
        }

        private void LateUpdate()
        {
            if (!tempRenderLine) return;
            if (activated)
            {
                position = transform.position;
                position.y += 1f;
                RenderArc();
            }
        }

        public void RenderArc()
        {
            lr.positionCount = resolution + 1;
            lr.SetPositions(CalculateArcArray());
       
        }

        Vector3[] CalculateArcArray() {
            Vector3[] arcArray = new Vector3[resolution + 1];
            radianAngle = Mathf.Deg2Rad * angle;
        
            maxDistance = ((Mathf.Pow(velocity + debugVelocity, 2) * Mathf.Sin(2*radianAngle)) / g) + 10;

            for(int i=0; i<=resolution;i++)
            {
                float t = (float)i / (float)resolution;
                arcArray[i] = CalculateArcPoint(t, maxDistance);
            }
            return arcArray;
        }
        Vector3 CalculateArcPoint(float t,float maxDistance)
        {
            float x = t * maxDistance;
            float y = x * Mathf.Tan(radianAngle) - ((g * x * x)/(2 * Mathf.Pow(velocity + debugVelocity, 2) * Mathf.Cos(radianAngle) * Mathf.Cos(radianAngle)));
            return (new Vector3(0, y) + position)+rotation*maxDistance*t;
        }
    }
}
