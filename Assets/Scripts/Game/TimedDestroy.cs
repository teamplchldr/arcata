﻿using UnityEngine;

namespace Assets.Scripts.Game {
    public class TimedDestroy : MonoBehaviour
    {
        public float TimeToLive = 5.0f;
        // Start is called before the first frame update
        void Start()
        {
            Destroy(gameObject,TimeToLive);
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
