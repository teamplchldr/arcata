﻿using UnityEngine;

namespace Assets.Scripts {
    public class DeprisExplosion : MonoBehaviour
    {
        bool isExplosionActive = false;
        Vector3 explosionDirection;

        void Update()
        {
            if (isExplosionActive)
            {
                var rigidBody = gameObject.AddComponent<Rigidbody>();
                rigidBody.AddForce(new Vector3(Random.onUnitSphere.x, 1, Random.onUnitSphere.z) * 200f);
                isExplosionActive = false;
            }
        }

        public void ActivateExplosion(Vector3 angle)
        {
            isExplosionActive = true;
            explosionDirection = angle;
        }
    }
}
