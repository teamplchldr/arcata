﻿using UnityEngine;

namespace Assets.Scripts.Game {
    public class TriggerDepris : MonoBehaviour
    {
        [SerializeField]
        bool killBuildingDebug;

        float despawnTime = float.MaxValue;

        void Update()
        {
            if (killBuildingDebug)
            {
                killBuildingDebug = false;
                TriggerExplosion();
            }
            if(despawnTime < Time.time)
            {
                Destroy(gameObject);
            }
        
        }

        void TriggerExplosion()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i).gameObject;
                var deprisExpl = child.AddComponent<DeprisExplosion>();
                deprisExpl.ActivateExplosion(transform.rotation.eulerAngles);

            }
            despawnTime = Time.time + 3f;
        }
    }
}
