﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArcataGameManager.cs" company="Exit Games GmbH">
//   Part of: Asteroid demo
// </copyright>
// <summary>
//  Game Manager for the Asteroid Demo
// </summary>
// <author>developer@exitgames.com</author>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections;
using Assets.Scripts.Game.UI;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Game.Managers {
    public class ArcataGameManager : MonoBehaviourPunCallbacks {
        public static ArcataGameManager Instance;

        public string IslandPrefabName;
        public string PlayerPrefabName;

        public Text InfoText;
        public string LobbysceneName = "Lobby";

        public ArcataGame.RoomState _state = ArcataGame.RoomState.Setup;

        public GameObject LocalPlayer { get; private set; }

        public GameObject PlayerUI;

        #region UNITY

        public void Awake() {
            Instance = this;
        }

        public override void OnEnable() {
            base.OnEnable();
            CountdownTimer.OnCountdownTimerHasExpired += OnCountdownTimerIsExpired;
        }

        public void Start() {
            InfoText.text = "Waiting for other players...";

            ArcataController.Instance.AnchorStatusDelegate += LocalPlayerAnchorStatusChanged;

            var props = new Hashtable {
                {ArcataGame.PLAYER_LOADED_LEVEL, true},
                {ArcataGame.PLAYER_ANCHOR_STATUS, (int) ArcataGame.AnchorStatus.NotPlaced},
                {ArcataGame.PLAYER_LOST_ANCHOR_TIMES, 0}
            };
            PhotonNetwork.LocalPlayer.SetCustomProperties(props);
        }

        public override void OnDisable() {
            base.OnDisable();
            CountdownTimer.OnCountdownTimerHasExpired -= OnCountdownTimerIsExpired;
        }

        private void OnCountdownTimerIsExpired() {
            StartGame();
        }

        #endregion

        #region COROUTINES

        private IEnumerator EndOfGame(string winner, int score) {
            var timer = 5.0f;

            while (timer > 0.0f) {
                InfoText.text =
                    $"Player {winner} won with {score} points.\n\n\nReturning to login screen in {timer.ToString("n2")} seconds.";

                yield return new WaitForEndOfFrame();

                timer -= Time.deltaTime;
            }

            PhotonNetwork.LeaveRoom();
        }

        #endregion

        #region PUN CALLBACKS

        public override void OnDisconnected(DisconnectCause cause) {
            SceneManager.LoadScene(LobbysceneName);
        }

        public override void OnLeftRoom() {
            PhotonNetwork.Disconnect();
        }

        public override void OnMasterClientSwitched(Player newMasterClient) {
            if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber) {
                //StartCoroutine(SpawnAsteroid());
            }
        }

        public override void OnPlayerLeftRoom(Player otherPlayer) {
            CheckEndOfGame();
        }

        //Something changed in synced properties, like player connected or lost a life
        public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps) {
            if (changedProps.ContainsKey(ArcataGame.PLAYER_REMANING_BUILDINGS)) {
                CheckEndOfGame();
                return;
            }


            if (changedProps.ContainsKey(ArcataGame.PLAYER_LOADED_LEVEL)) {
                if (CheckAllPlayerLoadedLevel()) {
                    InfoText.text = "All player connected, no anchors placed yet";
                }
            }

            if (changedProps.ContainsKey(ArcataGame.PLAYER_ANCHOR_STATUS)) {
                switch (_state) {
                    case ArcataGame.RoomState.Setup:
                        if (CheckAllPlayerLockedAnchor()) {
                            ChangeRoomState(ArcataGame.RoomState.InProgress, "Starting (Master)", "Starting (Client)");
                            StartCountdown();
                        }
                        else
                            SetupStep(targetPlayer);

                        break;
                    case ArcataGame.RoomState.InProgress:
                        if ((int) changedProps[ArcataGame.PLAYER_ANCHOR_STATUS] ==
                            (int) ArcataGame.AnchorStatus.LostTracking) {
                            //Put game on hold
                            ChangeRoomState(ArcataGame.RoomState.Paused, "A player lost tracking(Master)",
                                "A player lost tracking(Master)");
                        }

                        break;
                    case ArcataGame.RoomState.Paused:
                        if (CheckAllPlayerLockedAnchor()) {
                            ChangeRoomState(ArcataGame.RoomState.InProgress, "Resuming (Master)", "Resuming (Client)");
                            StartCountdown();
                        }

                        break;
                    case ArcataGame.RoomState.Ended:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private void SetupStep(Player targetPlayer) {
            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(ArcataGame.PLAYER_ANCHOR_STATUS,
                    out var outPlayerSpawedAnchor) &&
                (int) outPlayerSpawedAnchor == (int) ArcataGame.AnchorStatus.Locked) {
                InfoText.text = "Waiting for everyone else to spawn their anchor";
            }
            else if (!targetPlayer.IsLocal) {
                InfoText.text = "Someone else placed theirs, hurry up!";
            }
        }

        private static void StartCountdown() {
            if (PhotonNetwork.IsMasterClient) {
                var props = new Hashtable {
                    {CountdownTimer.CountdownStartTime, (float) PhotonNetwork.Time}
                };
                PhotonNetwork.CurrentRoom.SetCustomProperties(props);
            }
        }

        public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged) {
            base.OnRoomPropertiesUpdate(propertiesThatChanged);

            if (propertiesThatChanged.ContainsKey(ArcataGame.ROOM_GAME_STATE))
            {
                switch ((ArcataGame.RoomState)propertiesThatChanged[ArcataGame.ROOM_GAME_STATE]) {
                    case ArcataGame.RoomState.Setup:
                        //All players has joined but has not yet locked their anchors
                        break;
                    case ArcataGame.RoomState.InProgress:
                        //All players are ready for gameplay
                        break;
                    case ArcataGame.RoomState.Paused:
                        //Player lost tracking or paused, halt all gameplay
                        break;
                    case ArcataGame.RoomState.Ended:
                        //Game over
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                //Starting, occurs after setup but before OnCountdownTimerHasExpired
            }

            if (propertiesThatChanged.ContainsKey(CountdownTimer.CountdownStartTime)) {
                //picked up in Countdown script that is attached to this game
                //Use the delegate CountdownTimer.OnCountdownTimerHasExpired
            }
        }

        #endregion

        private void ChangeRoomState(ArcataGame.RoomState state, string MasterInfo, string ClientInfo) {
            if (PhotonNetwork.IsMasterClient) {
                InfoText.text = MasterInfo;

                _state = state;
                var props = new Hashtable {
                    {ArcataGame.ROOM_GAME_STATE, (int) _state}
                };
                PhotonNetwork.CurrentRoom.SetCustomProperties(props);
            }
            else {
                InfoText.text = ClientInfo;
            }
        }

        public void StartGame() {
            InfoText.text = PhotonNetwork.IsMasterClient ? "StartGame (Master)!" : "StartGame (Client)!";

            //Spawn the player
            LocalPlayer = PhotonNetwork.Instantiate(
                PlayerPrefabName,
                PhotonNetwork.LocalPlayer.GetPlayerNumber() == 1 ?
                ArcataController.Instance.WorldAnchor.Island1Transform.position :
                ArcataController.Instance.WorldAnchor.Island2Transform.position,
                Quaternion.identity, 
                0);

            PlayerUI.SetActive(true);
            PlayerUI.GetComponentInChildren<Joystick>().Player = LocalPlayer.GetComponentInChildren<PlayerMovement>();
            
            LocalPlayer.GetComponentInChildren<LaunchArcRenderer>().forcePuller = PlayerUI.GetComponentInChildren<ForcePuller>();
            LocalPlayer.GetComponentInChildren<LaunchArcRenderer>().Ready();

            LocalPlayer.GetComponentInChildren<SpoonCharge>().forcePuller = PlayerUI.GetComponentInChildren<ForcePuller>();
            LocalPlayer.GetComponentInChildren<SpoonCharge>().Ready();

            LocalPlayer.GetComponentInChildren<PlayerController>().Ready();

        }

        private static bool CheckAllPlayerLoadedLevel() {
            foreach (var p in PhotonNetwork.PlayerList) {
                if (p.CustomProperties.TryGetValue(ArcataGame.PLAYER_LOADED_LEVEL, out var playerLoadedLevel)) {
                    if ((bool) playerLoadedLevel) {
                        continue;
                    }
                }

                return false;
            }

            return true;
        }

        private static bool CheckAllPlayerLockedAnchor() {
            foreach (var p in PhotonNetwork.PlayerList) {
                if (p.CustomProperties.TryGetValue(ArcataGame.PLAYER_ANCHOR_STATUS, out var o)) {
                    if ((int) o == (int) ArcataGame.AnchorStatus.Locked) {
                        continue;
                    }
                }

                return false;
            }

            return true;
        }

        //todo: update for arcata
        private void CheckEndOfGame() {
            var allDestroyed = true;

            foreach (var p in PhotonNetwork.PlayerList) {
                if (p.CustomProperties.TryGetValue(ArcataGame.PLAYER_REMANING_BUILDINGS, out var remaninglives)) {
                    if ((int) remaninglives > 0) {
                        allDestroyed = false;
                        break;
                    }
                }
            }

            if (allDestroyed) {
                if (PhotonNetwork.IsMasterClient) {
                    StopAllCoroutines();
                }

                var winner = "";
                var score = -1;

                foreach (var p in PhotonNetwork.PlayerList) {
                    if (p.GetScore() > score) {
                        winner = p.NickName;
                        score = p.GetScore();
                    }
                }

                StartCoroutine(EndOfGame(winner, score));
            }
        }

        public void LocalPlayerAnchorStatusChanged(ArcataGame.AnchorStatus status) {
            Debug.Log($"LocalPlayerAnchorStatusChanged {status}");
            var props = new Hashtable {
                {ArcataGame.PLAYER_ANCHOR_STATUS, (int) status}
            };
            PhotonNetwork.LocalPlayer.SetCustomProperties(props);
        }
    }
}