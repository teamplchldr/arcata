﻿using UnityEngine;

namespace Assets.Scripts.Game.Managers
{
    public class ArcataGame
    {
        public enum AnchorStatus
        {
            NotPlaced = 0,
            Placed,
            Locked,
            LostTracking
        }

        public enum RoomState
        {
            Setup,
            InProgress,
            Paused,
            Ended,
        }


        public const float PLAYER_RESPAWN_TIME = 4.0f;
        public const int PLAYER_STARTING_BUILDING_COUNT = 3;
        
        //Hash key
        public const string LOBBY_PLAYER_READY = "LobbyPlayerReady";
        public const string PLAYER_REMANING_BUILDINGS = "PlayerLives";
        public const string PLAYER_LOADED_LEVEL = "PlayerLoadedLevel";
        public const string PLAYER_ANCHOR_STATUS = "PlayerAnchorStatus";
        public const string PLAYER_LOST_ANCHOR_TIMES = "PlayerLostAnchorTimes";

        public const string ROOM_GAME_STATE = "RoomGameState";



        public static Color GetColor(int colorChoice)
        {
            switch (colorChoice)
            {
                case 0: return Color.red;
                case 1: return Color.green;
                case 2: return Color.blue;
                case 3: return Color.yellow;
                case 4: return Color.cyan;
                case 5: return Color.grey;
                case 6: return Color.magenta;
                case 7: return Color.white;
            }

            return Color.black;
        }
    }
}