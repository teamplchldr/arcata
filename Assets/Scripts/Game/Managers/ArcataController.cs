﻿//-----------------------------------------------------------------------
// <copyright file="HelloARController.cs" company="Google">
//
// Copyright 2017 Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// </copyright>
//-----------------------------------------------------------------------

using System;
using Assets.Scripts.Game.UI;
using GoogleARCore;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Game.Managers
{
#if UNITY_EDITOR
    // Set up touch input propagation while using Instant Preview in the editor.
    using Input = InstantPreviewInput;

#endif

    /// <summary>
    /// Controls the HelloAR example.
    /// </summary>
    public class ArcataController : MonoBehaviour
    {
        public static ArcataController Instance;

        public delegate void AnchorStatusChanged(ArcataGame.AnchorStatus status);

        public AnchorStatusChanged AnchorStatusDelegate;


        /// <summary>
        /// The first-person camera being used to render the pass through camera image (i.e. AR
        /// background).
        /// </summary>
        public Camera FirstPersonCamera;

        /// <summary>
        /// A prefab for tracking and visualizing detected planes.
        /// </summary>
        public GameObject DetectedPlanePrefab;

        /// <summary>
        /// A model to place when a ray-cast from a user touch hits a plane.
        /// </summary>
        public GameObject WorldAnchorPrefab;

        public WorldAnchor WorldAnchor;
        public WorldAnchorSetup WorldAnchorSetup = null;

        public float TimeAfterTackingWasLost { get; private set; }
        public float LostTrackingTimeOut = 15f;

        public ArcataGame.AnchorStatus AnchorState
        {
            get => _anchorState;
            private set
            {
                //Ugly but with tracking issues this might be needed
                if (_anchorState == value)
                {
                    return;
                }

                _previousAnchorState = _anchorState;
                _anchorState = value;
                AnchorStatusDelegate.Invoke(value);
            }
        }


        private ArcataGame.AnchorStatus _anchorState = ArcataGame.AnchorStatus.NotPlaced;
        private ArcataGame.AnchorStatus _previousAnchorState = ArcataGame.AnchorStatus.NotPlaced;

        /// <summary>
        /// True if the app is in the process of quitting due to an ARCore connection error,
        /// otherwise false.
        /// </summary>
        private bool m_IsQuitting;

        public void Awake()
        {
            Instance = this;
        }

        public void Start()
        {
            WorldAnchorSetup.GetComponentInChildren<Button>().onClick.AddListener(AnchorSetupLocked);
        }

        /// <summary>
        /// The Unity Update() method.
        /// </summary>
        public void Update()
        {
            _UpdateApplicationLifecycle();

            switch (_anchorState)
            {
                case ArcataGame.AnchorStatus.NotPlaced:
                    PlaceAnchor();
                    break;
                case ArcataGame.AnchorStatus.Placed:
                case ArcataGame.AnchorStatus.Locked:
                    if (Session.Status == SessionStatus.LostTracking)
                    {
                        TimeAfterTackingWasLost = 0f;
                        AnchorState = ArcataGame.AnchorStatus.LostTracking;
                    }

                    break;
                case ArcataGame.AnchorStatus.LostTracking:
                    if (Session.Status != SessionStatus.LostTracking)
                    {
                        AnchorState = _previousAnchorState;
                    }
                    else
                    {
                        TimeAfterTackingWasLost += Time.deltaTime;
                        if (TimeAfterTackingWasLost >= LostTrackingTimeOut) {
                            ArcataGameManager.Instance.InfoText.text = $"Lost tracking,deleting anchor";
                            ResetAnchor();
                        }
                        ArcataGameManager.Instance.InfoText.text = $"Lost tracking, deleting in ({(LostTrackingTimeOut - TimeAfterTackingWasLost):n2})";
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void ResetAnchor()
        {
            _ShowAndroidToastMessage("You lost anchor, reset it you bum");

            if (WorldAnchor)
            {
                //todo: hide anchor instead of removing to not loose progress
                Destroy(WorldAnchor);
                WorldAnchor = null;
            }

            AnchorState = ArcataGame.AnchorStatus.NotPlaced;
        }

        private void PlaceAnchor()
        {
            // If the player has not touched the screen, we are done with this update.
            Touch touch;
            if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
            {
                return;
            }

            // Should not handle input if the player is pointing on UI.
            if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
            {
                return;
            }

            //Raycast missed
            if (!Frame.Raycast(touch.position.x, touch.position.y, TrackableHitFlags.PlaneWithinPolygon, out var hit))
            {
                return;
            }

            // Use hit pose and camera pose to check if hittest is from the
            // back of the plane, if it is, no need to create the anchor.
            if ((hit.Trackable is DetectedPlane) &&
                Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position,
                    hit.Pose.rotation * Vector3.up) < 0)
            {
                Debug.Log("Hit at back of the current DetectedPlane");
            }
            else
            {
                // Instantiate Andy model at the hit pose.
                WorldAnchor = Instantiate(WorldAnchorPrefab, hit.Pose.position, hit.Pose.rotation).GetComponent<WorldAnchor>();

                // Create an anchor to allow ARCore to track the hitpoint as understanding of
                // the physical world evolves.
                var anchor = hit.Trackable.CreateAnchor(hit.Pose);

                // Make Andy model a child of the anchor.
                WorldAnchor.transform.parent = anchor.transform;

                WorldAnchorSetup.HeightSlider.value = WorldAnchor.transform.localPosition.y;
                WorldAnchorSetup.ScaleSlider.value = WorldAnchor.transform.localScale.y;
                WorldAnchorSetup.RotationSlider.value = WorldAnchor.transform.localEulerAngles.y;
                WorldAnchorSetup.gameObject.SetActive(true);

                AnchorState = ArcataGame.AnchorStatus.Placed;
            }
        }

        private void FixedUpdate()
        {
            if (AnchorState == ArcataGame.AnchorStatus.Placed && Session.Status != SessionStatus.LostTracking)
            {
                WorldAnchor.UpdateWorldAnchor();
            }
        }

        /// <summary>
        /// Check and update the application lifecycle.
        /// </summary>
        private void _UpdateApplicationLifecycle()
        {
            // Exit the app when the 'back' button is pressed.
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }

            // Only allow the screen to sleep when not tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                const int lostTrackingSleepTimeout = 15;
                Screen.sleepTimeout = lostTrackingSleepTimeout;
            }
            else
            {
                Screen.sleepTimeout = SleepTimeout.NeverSleep;
            }

            if (m_IsQuitting)
            {
                return;
            }

            // Quit if ARCore was unable to connect and give Unity some time for the toast to
            // appear.
            if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
            {
                _ShowAndroidToastMessage("Camera permission is needed to run this application.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
            else if (Session.Status.IsError())
            {
                _ShowAndroidToastMessage(
                    "ARCore encountered a problem connecting.  Please start the app again.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
        }

        /// <summary>
        /// Actually quit the application.
        /// </summary>
        private void _DoQuit()
        {
            Application.Quit();
        }

        /// <summary>
        /// Show an Android toast message.
        /// </summary>
        /// <param name="message">Message string to show in the toast.</param>
        private void _ShowAndroidToastMessage(string message)
        {
            var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            var unityActivity =
                unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                var toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    var toastObject =
                        toastClass.CallStatic<AndroidJavaObject>(
                            "makeText", unityActivity, message, 0);
                    toastObject.Call("show");
                }));
            }
        }

        public void AnchorSetupLocked()
        {
            AnchorState = ArcataGame.AnchorStatus.Locked;
        }
    }
}