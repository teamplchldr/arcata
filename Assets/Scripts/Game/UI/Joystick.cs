﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Game.UI {
    public class Joystick : MonoBehaviour
    {
        [Header("UI Setup")]
        public bool allowMouseInput = false;
        public RectTransform Stick;
        public RectTransform Border;
        private float MaxUIDistance = 50.0f;
        private int _CurrectControllingTouch = -1;
        private Vector2 _point;
        public Vector2 AbsDeadZones;

        private readonly int NOINPUT = -1;
        private readonly int MOUSEINPUT = 1;

        public PlayerMovement Player;


        // Update is called once per frame
        private void Update()
        {
            if (Input.touchCount > 0)
            {
                foreach (var touch in Input.touches)
                {
                    //If we have a controlling touch already then there is no need to care about the other ones
                    if (_CurrectControllingTouch != NOINPUT && _CurrectControllingTouch != touch.fingerId)
                    {
                        continue;
                    }

                    // Handle finger movements based on TouchPhase
                    switch (touch.phase)
                    {
                        //When a touch has first been detected, change the message and record the starting position
                        case TouchPhase.Began:
                            if (_CurrectControllingTouch == NOINPUT &&
                                EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                            {
                                _CurrectControllingTouch = touch.fingerId;
                            }

                            break;

                        //Determine if the touch is a moving touch
                        case TouchPhase.Moved:
                            _point = touch.position;
                            break;

                        case TouchPhase.Ended:
                            _CurrectControllingTouch = NOINPUT;
                            break;
                        case TouchPhase.Stationary:
                            break;
                        case TouchPhase.Canceled:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }


            if (!allowMouseInput && _CurrectControllingTouch == NOINPUT)
            {
                ResetStickPos();
                return;
            }

            //Mouse pressed this frame
            if (Input.GetMouseButtonDown(0) && EventSystem.current.IsPointerOverGameObject())
            {
                _CurrectControllingTouch = MOUSEINPUT;
            }
            //Mouse pressed previous frames and is still down
            else if (Input.GetMouseButton(0) && _CurrectControllingTouch == MOUSEINPUT)
            {
                _point = Input.mousePosition;
            }
            //Mouse released this frame
            else if (Input.GetMouseButtonUp(0) &&
                     _CurrectControllingTouch == MOUSEINPUT)
            {
                _CurrectControllingTouch = NOINPUT;
                ResetStickPos();
            }
        }

        //Move stick to central point
        private void ResetStickPos()
        {
            Stick.transform.position = Border.position;
            Player.RotateWheels(0,true);
        }

        //Do the character stuff, must be fixedUpdate otherwise there is jitter
        private void FixedUpdate()
        {
            if (_CurrectControllingTouch == NOINPUT){
                return;
            }

            var offset = _point - new Vector2(Border.position.x, Border.position.y);

            //Clamp the values to -1 to 1
            //MovePlayer(Vector2.ClampMagnitude(offset, 1.0f));
            Player.MovePlayer(offset.normalized);

            //Move stick but keep it within boundaries
            Stick.transform.position = (Vector2)Border.position + Vector2.ClampMagnitude(offset, MaxUIDistance);
        }

    }
}