﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Game.UI {
    public class ForcePuller : MonoBehaviour
    {
        [Header("UI Setup")]
        //private float MaxUIDistance = 50.0f;
        private int _CurrectControllingTouch = NOINPUT;
        private Vector2 _point_current;
        private Vector2 _point_start;
        private readonly float percentageOfScreenToMaxPower = 0.6f;

        public float ScreenDividerPercentage = 0.6f;
        public float MinimumPowerToFire = 0.6f;

        private static readonly int NOINPUT = -1;
        private static readonly int MOUSEINPUT = 1;

        [Header("Gameobjects")]
        public Image PullStartImage;
        public Image PullOngoingImage;
        public Text PullOngoingText;
        public Image PullForceBarImage;
        public Image PullForceFrameImage;
    
        public delegate void ReleaseCharge(float force);
        public event ReleaseCharge OnReleaseCharge;
        public delegate void Aiming(float force);
        public event Aiming OnAiming;

        // Start is called before the first frame update
        private void Start()
        {
            Debug.Assert(PullStartImage != null, "PullStartImage is unassigned!");
            Debug.Assert(PullOngoingText != null, "PullOngoingText is unassigned!");
            Debug.Assert(PullForceBarImage != null, "PullForceBarImage is unassigned!");
            Debug.Assert(PullForceFrameImage != null, "PullForceFrameImage is unassigned!");

            SetUIAlpha(0);
        }

        private void SetUIAlpha(float Alpha)
        {
            if (Alpha <= 0)
            {
                PullStartImage.gameObject.SetActive(true);
                PullOngoingImage.gameObject.SetActive(false);

                PullForceBarImage.gameObject.SetActive(false);
                PullForceFrameImage.gameObject.SetActive(false);
            }
            else
            {
                PullStartImage.gameObject.SetActive(false);
                PullOngoingImage.gameObject.SetActive(true);

                PullForceBarImage.gameObject.SetActive(true);
                PullForceFrameImage.gameObject.SetActive(true);

                //PullStartImage.transform.position = _point_start;

                //PullStartImage.GetComponent<Image>().CrossFadeAlpha(Alpha, 0, true);
                PullForceBarImage.GetComponent<Image>().CrossFadeAlpha(Alpha * 2, 0, true);
                //PullForceFrameImage.GetComponent<Image>().CrossFadeAlpha(Alpha, 0, true);
            }
        }

        // Update is called once per frame
        private void Update()
        {
            if (Input.touchCount > 0)
                TouchControl();
            else
                MouseControl();
        }

        private void MouseControl()
        {
            //Mouse pressed this frame
            if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                _point_start = Input.mousePosition;
                _point_current = Input.mousePosition;
            
                //Is point valid and withing start area?
                if (IsOutsideStartArea(_point_current.y))
                    return;

                _CurrectControllingTouch = MOUSEINPUT;
            }
            //Mouse pressed previous frames and is still down
            else if (Input.GetMouseButton(0) && _CurrectControllingTouch == MOUSEINPUT)
            {
                _point_current = Input.mousePosition;
                if (DragDistance() > 0)
                    OnAiming(DragDistance());
            }
            //Mouse released this frame
            else if (Input.GetMouseButtonUp(0) && _CurrectControllingTouch == MOUSEINPUT)
            {
                _CurrectControllingTouch = NOINPUT;
                SetUIAlpha(0);
                Fire();
            }
        }

        private void TouchControl()
        {
            foreach (var touch in Input.touches)
            {
                //If we have a controlling touch already then there is no need to care about the other ones
                if (_CurrectControllingTouch != NOINPUT && _CurrectControllingTouch != touch.fingerId)
                {
                    continue;
                }

                // Handle finger movements based on TouchPhase
                switch (touch.phase)
                {
                    //When a touch has first been detected, change the message and record the starting position
                    case TouchPhase.Began:
                        if (_CurrectControllingTouch == NOINPUT &&
                            !EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                        {
                            _point_current = touch.position;
                            _point_start = _point_current;

                            //Is touch valid and withing start area?
                            if (!IsOutsideStartArea(_point_current.y))
                                break;
                        

                            _CurrectControllingTouch = touch.fingerId;
                        }

                        break;

                    //Determine if the touch is a moving touch
                    case TouchPhase.Moved:
                        _point_current = touch.position;
                        if (DragDistance() > 0)
                            OnAiming(DragDistance());
                        break;

                    case TouchPhase.Ended:
                        _CurrectControllingTouch = NOINPUT;
                        SetUIAlpha(0);

                        Fire();
                        break;
                    case TouchPhase.Stationary:
                        if (DragDistance() > 0)
                            OnAiming(DragDistance());
                        break;
                    case TouchPhase.Canceled:
                        _CurrectControllingTouch = NOINPUT;
                        SetUIAlpha(0);

                        Fire();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return;
        }

        //Do the character stuff, must be fixedUpdate otherwise there is jitter
        private void FixedUpdate()
        {
            if (_CurrectControllingTouch == NOINPUT)
            {
                return;
            }

            //0 to 1
            var dragPercentage = DragDistance();


            if (IsOutsideStartArea(_point_current.y))
            {
                PullOngoingText.text =
                    dragPercentage < MinimumPowerToFire
                        ? $"Not enough force:\n{(dragPercentage * 100).ToString("F1")}%\nRelease To Cancel"
                        : $"Dangerous amount of Force:\n{(dragPercentage * 100).ToString("F1")}%\nRelease To FIRE";
            }
            else
                PullOngoingText.text = "Need to drag out-\nside of this box!";

            //Scale the PullForce Bar between 0 to 1
            PullForceBarImage.transform.localScale = new Vector3(1, dragPercentage, 1);

            SetUIAlpha(dragPercentage);
        }

        private float DragDistance()
        {
            var dragDistance = _point_start.y - _point_current.y;

            dragDistance /= Camera.main.pixelHeight;
            dragDistance /= percentageOfScreenToMaxPower;

            //Should only occur in editor
            dragDistance = Mathf.Min(dragDistance, 1.0f);

            return dragDistance;
        }

        private bool IsOutsideStartArea(float point)
        {
            var screenHitPercentage = point / Camera.main.pixelHeight;
            //Debug.Log($"screenHitPercentage{screenHitPercentage} <= ScreenDividerPercentage{ScreenDividerPercentage}={screenHitPercentage <= ScreenDividerPercentage}");
            return screenHitPercentage <= ScreenDividerPercentage;
        }

        void Fire()
        {
            OnReleaseCharge(-1f);
            if (IsOutsideStartArea(_point_start.y))
            {
                Debug.Log("Start Was outside start - Cancelled");
                return;
            }

            if (!IsOutsideStartArea(_point_current.y))
            {
                Debug.Log("Current was inside start - Cancelled");
                return;
            }

            var dragDistance = DragDistance();
        


            Debug.Log(
                dragDistance < MinimumPowerToFire
                    ? $"Not enough force to fire{(dragDistance).ToString("F1")}"
                    : $"Shot with {(dragDistance).ToString("F1")}");
            if(dragDistance > MinimumPowerToFire)
            {
                OnReleaseCharge(dragDistance);
            }
        }
    
    }
}