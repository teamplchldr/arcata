﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Game.UI {
    public class HeightController : MonoBehaviour
    {
        // Start is called before the first frame update

        private GameObject _Island = null;
        public Text TextObject = null;
        private bool firstFrame = false;
        private float targetHeight = 0;


        void Start()
        {
            gameObject.SetActive(false);
        }


        // Update is called once per frame
        void Update()
        {
            if(_Island == null)
                return;
            if (!firstFrame)
            {
                firstFrame = !firstFrame;
                GetComponentInChildren<Slider>().value = _Island.transform.position.y;
                SliderValueChanged(_Island.transform.position.y);
            }

            _Island.transform.position = Vector3.Lerp(_Island.transform.position, new Vector3(_Island.transform.position.x, targetHeight, _Island.transform.position.z), Time.deltaTime);

            TextObject.text = @"Set height!
Current: " + targetHeight.ToString("F2");



        }

        public void Activate(GameObject island)
        {
            _Island = island;
            gameObject.SetActive(true);
        }

        public void SliderValueChanged(float value)
        {
            targetHeight = value;
        }

        public void ButtonPressed()
        {
            _Island = null;
            gameObject.SetActive(false);
        }
    }
}
