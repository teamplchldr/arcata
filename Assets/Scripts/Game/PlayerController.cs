﻿using Assets;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using Assets.Scripts.Game.UI;
using Assets.Scripts.Game.Managers;
using UnityEngine;
using Photon.Pun;

public class PlayerController : MonoBehaviourPunCallbacks, IPunObservable {
    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;

    public SpoonCharge spoonCharge;
    public LaunchArcRenderer launchArcRenderer;
    public float CatapultScale = 0.1f;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        throw new System.NotImplementedException();
    }

    public override void OnEnable() {
        base.OnEnable();
    }

    public void Ready() {
        gameObject.transform.parent = ArcataController.Instance.WorldAnchor.transform;
        gameObject.transform.localScale = new Vector3(CatapultScale, CatapultScale, CatapultScale) *
                               ArcataController.Instance.WorldAnchor.TargetScale;
        if (true) {
            PlayerController.LocalPlayerInstance = this.gameObject;

            // Get all required Event related scripts. Do it from unity interface
            /*forcePuller = GetComponentInChildren<ForcePuller>();
            spoonCharge = GetComponentInChildren<SpoonCharge>();
            launchArcRenderer = GetComponentInChildren<LaunchArcRenderer>();*/
            //uiGameObject.SetActive(true);
        }
        else {
            this.enabled = false;
            //uiGameObject.SetActive(false);
        }

        //DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update() { }
}