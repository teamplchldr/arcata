﻿using Assets.Scripts.Game.UI;
using Photon.Pun;
using UnityEngine;

namespace Assets.Scripts {
    public class SpoonCharge : MonoBehaviourPunCallbacks, IPunObservable
    {
    
        [Header("Projectile")]
        public GameObject Projectile;

        private PlayerController playerController;

        private float transitionTimer = 0;
    
        private readonly float rechargeTime = 2f;
        private readonly float releaseMultiplier = 4f;
        private readonly float timeAdjustment = 0.1f; // due to unreliable fps adjustment is needed to make sure it finishes.

        private bool releaseCharge;
        private bool reloadCharge;
        private bool hasFired = true;
        private readonly Quaternion unloadedPosition = Quaternion.Euler(-90f, 0, 0); // decharged = spoon is up after firing. needs recharge.
        private readonly Quaternion loadedPosition = Quaternion.Euler(-180f, 0, 0); // charged = is ready to fire.
        public ForcePuller forcePuller;

        public Transform projectileSpawner;
        private float projectileForce;
        private float projectileActualForce;
        private Vector3 direction;
        private readonly float projectileMult = 20;
        private readonly float projectileScale = 20f;
        private readonly Vector3 arcUpDirection = new Vector3(0, 0.5f, 0);

        public delegate void AimingSpoon(float projectileForce, Vector3 up);
        public event AimingSpoon OnCharging;

        public void Ready()
        {
            forcePuller.OnReleaseCharge += Fire;
            forcePuller.OnAiming += UpdateAim;
            reloadCharge = true;
        }

        private void UpdateAim(float force)
        {
            projectileActualForce = force;
            projectileForce = Mathf.Pow(force, 2) * projectileMult;
            OnCharging(projectileForce, arcUpDirection);
        }

        private void OnDisable()
        {
            base.OnDisable();
            if (forcePuller != null)
            {
                forcePuller.OnReleaseCharge -= Fire;
                forcePuller.OnAiming -= UpdateAim;
            }
        }

        void Update()
        {
            // Check if rotation is finished before allowing the other to be used
            releaseCharge = (releaseCharge && hasFired) ? false : releaseCharge;
            reloadCharge = (reloadCharge && !hasFired) ? false : reloadCharge;

            if (releaseCharge && !hasFired)
            {
                RotateForwards();
            }
        
            if (reloadCharge && hasFired)
            {
                RotateBackwards();
            }
        }

        public void Fire(float force)
        {
            if (force == -1f)
                return;

            projectileActualForce = force;
            projectileForce = Mathf.Pow(force, 2) * projectileMult;

            //Debug.Log("Force: " + projectileForce + "N");
            if (!hasFired)
            {
                releaseCharge = true;
            }
            else
            {
                Debug.Log("You need to reload before you fire");
            }
        } 

        void RotateBackwards()
        {
      
            transform.localRotation = Quaternion.Slerp(
                unloadedPosition, 
                loadedPosition, 
                transitionTimer / rechargeTime);
            transitionTimer += Time.deltaTime;
            if (transitionTimer - timeAdjustment > rechargeTime)
            {
                transitionTimer = 0;
                reloadCharge = false;
                hasFired = false;
            }
        }

        void RotateForwards()
        {

            // Calculation to slow down the transition based on the projectile force.
            var projSpeedTransTimer = Mathf.Pow(projectileActualForce * 2, 2);

            // following mathf pow function inside the Slerp is just a the multiplicative with the timer... 
            // it increases speed by n times (it reaches 1 - completion - n times faster)
            // this means that a shot in this example would take 1/n seconds. n = shootMult 
            // the pow function takes one second (because lim(x->1) x^n == 1 so it'll still be one second) just less linear
            transform.localRotation = Quaternion.Slerp(
                loadedPosition, 
                unloadedPosition, 
                Mathf.Pow(transitionTimer * releaseMultiplier, 2) * projSpeedTransTimer);
            transitionTimer += Time.deltaTime;

            if (transitionTimer - timeAdjustment > (1 / releaseMultiplier) / projSpeedTransTimer)
            {
                transitionTimer = 0;

                var force = (direction + arcUpDirection).normalized * projectileForce; // Assume ForceMode.Impulse
                var torque = 0;
                object[] instantiationData = {force, torque, true};
                PhotonNetwork.Instantiate("Prefabs/" + Projectile.name, projectileSpawner.position, transform.parent.rotation);
                // projectileSpawner = transform.position + new Vector3(0f,1f,0f)
                /*
                go.transform.localScale = new Vector3(projectileScale,projectileScale,projectileScale);
            
                //rigid.SetDensity(0.5f); Potential way to control the feel of gravity if not "rigid.SetDrag" or something
                direction = -transform.TransformDirection(transform.forward).normalized;

                var rigid = go.GetComponent<Rigidbody>();
                rigid.AddForce((direction + arcUpDirection).normalized * projectileForce, ForceMode.Impulse);
                rigid.angularDrag = 0;
                */
                releaseCharge = false;
                hasFired = true;
                reloadCharge = true;
            }
        
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                // We own this player: send the others our data
                stream.SendNext(releaseCharge);
                stream.SendNext(reloadCharge);
               
            }
            else
            {
                // Network player, receive data
                this.releaseCharge = (bool)stream.ReceiveNext();
                this.reloadCharge = (bool)stream.ReceiveNext();
            }
        }
    }
}
