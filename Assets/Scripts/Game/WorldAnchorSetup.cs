﻿using Assets.Scripts.Game.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class WorldAnchorSetup : MonoBehaviour
    {
        public Slider HeightSlider = null;
        public Slider ScaleSlider = null;
        public Slider RotationSlider = null;

        // Start is called before the first frame update
        void Start()
        {
            Debug.Assert(HeightSlider != null, "HeightSlider is null");
            Debug.Assert(ScaleSlider != null, "ScaleSlider is null");
            Debug.Assert(RotationSlider != null, "RotationSlider is null");

            HeightSlider.onValueChanged.AddListener(OnHeightChanged);
            ScaleSlider.onValueChanged.AddListener(OnScaleChanged);
            RotationSlider.onValueChanged.AddListener(OnRotationChanged);

            ArcataController.Instance.WorldAnchorSetup = this;

            this.gameObject.SetActive(false);

        }

        void OnHeightChanged(float value)
        {
            ArcataController.Instance.WorldAnchor.TargetHeight = value;
        }

        void OnScaleChanged(float value)
        {
            ArcataController.Instance.WorldAnchor.TargetScale = value;
        }

        void OnRotationChanged(float value)
        {
            ArcataController.Instance.WorldAnchor.TargetRotation = value;
        }
    }
}
